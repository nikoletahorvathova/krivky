#pragma once
#include <QtWidgets>
#include <QWidget>


class QtClass : public QWidget{
	Q_OBJECT
protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

public:
	QtClass(QWidget *parent = Q_NULLPTR);
	~QtClass();

	QImage *obrazok;
	QRgb* data;
	int row;

	QColor farbaHrany;
	QVector<QPoint> body;

	void newImage(int x, int y);
	void initImage();
	void vymaz();
	void setPixel(int x, int y, QColor color);
	void vyber_farbu();

	QPoint getCasteljauPoint(int r, int i, double t);
	void drawCasteljau();

	void BSpline(double t);
	void drawBSpline();
};