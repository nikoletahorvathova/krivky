#include "Krivky.h"

Krivky::Krivky(QWidget *parent)
	: QMainWindow(parent){
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->widget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);

	widget.newImage(800, 500);
	widget.initImage();
}

void Krivky::on_kresli_clicked(){
	if(ui.bezier->isChecked()){
		widget.drawCasteljau();
	}
}

void Krivky::on_vymaz_clicked(){
	widget.newImage(800, 500);
	widget.vymaz();
	widget.initImage();
}
