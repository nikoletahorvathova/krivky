#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_Krivky.h"
#include "QtClass.h"

class Krivky : public QMainWindow
{
	Q_OBJECT

public:
	Krivky(QWidget *parent = Q_NULLPTR);

private:
	Ui::KrivkyClass ui;
	QtClass widget;

public slots:
	void on_kresli_clicked();
	void on_vymaz_clicked();

};
