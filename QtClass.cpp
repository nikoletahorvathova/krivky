#include "QtClass.h"


QtClass::QtClass(QWidget *parent)
	: QWidget(parent){
	vyber_farbu();
}

QtClass::~QtClass(){
}

void QtClass::mousePressEvent(QMouseEvent* event) {
	if (event->button() == Qt::LeftButton) {
		body.push_back(event->pos());
		setPixel(event->pos().x(), event->pos().y(), farbaHrany);
		setPixel(event->pos().x() + 1, event->pos().y(), farbaHrany);
		setPixel(event->pos().x(), event->pos().y() + 1, farbaHrany);
		setPixel(event->pos().x(), event->pos().y() - 1, farbaHrany);
		setPixel(event->pos().x() - 1, event->pos().y(), farbaHrany);
		setPixel(event->pos().x() + 1, event->pos().y() + 1, farbaHrany);
	}
	update();
}

void QtClass::paintEvent(QPaintEvent *event) {
	QPainter painter(this);
	QRect area = event->rect();
	painter.drawImage(area, *obrazok, area);
}

void QtClass::newImage(int x, int y) {
	obrazok = new QImage(x, y, QImage::Format_RGB32);
	obrazok->fill(Qt::white);
	this->setMinimumSize(obrazok->size());

	update();
}

void QtClass::initImage() {
	this->resize(obrazok->size());
	this->setMinimumSize(obrazok->size());

	row = obrazok->bytesPerLine() / 4;
	data = (QRgb *)obrazok->bits();
	update();
}

void QtClass::setPixel(int x, int y, QColor color) {
	data[x + y*row] = color.rgb();
}

void QtClass::vymaz(){
	body.clear();
}

void QtClass::vyber_farbu() {
	farbaHrany = QColorDialog::getColor(Qt::black, this, "Vyber farbu hranice", QColorDialog::DontUseNativeDialog);
}

QPoint QtClass::getCasteljauPoint(int r, int i, double t) {
	QPoint p, p1, p2;

	if (r == 0) {
		return body[i];
	}

	p1 = getCasteljauPoint(r - 1, i, t);
	p2 = getCasteljauPoint(r - 1, i + 1, t);

	p.setX((int)((1 - t) * p1.x() + t * p2.x()));
	p.setY((int)((1 - t) * p1.y() + t * p2.y()));

	return p;
}

void QtClass::drawCasteljau(){
	QPoint tmp;

	for (double t = 0; t< 1.; t += 0.0001) {
		tmp = getCasteljauPoint(body.size() - 1, 0, t);
		setPixel(tmp.x(), tmp.y(), farbaHrany);
	}

	update();
}

void QtClass::BSpline(double t){
	QVector<QPoint> pomocne_body;
	QVector<QPoint> vektor;


	for (int i = 1; i < body.size()-1; i++) {
		pomocne_body.push_back(QPoint((body[i - 1].x() + 4 * body[i].x() + body[i+1].x()) / 6.0,(body[i - 1].y() + 4 * body[i].y() + body[i+1].y()) / 6.0));
		setPixel(pomocne_body[i-1].x(), pomocne_body[i-1].y(), Qt::red);
		setPixel(pomocne_body[i-1].x() + 1, pomocne_body[i-1].y(), Qt::red);
		setPixel(pomocne_body[i-1].x(), pomocne_body[i-1].y() + 1, Qt::red);
		setPixel(pomocne_body[i-1].x(), pomocne_body[i-1].y() - 1, Qt::red);
		setPixel(pomocne_body[i-1].x() - 1, pomocne_body[i-1].y(), Qt::red);
		setPixel(pomocne_body[i-1].x() + 1, pomocne_body[i-1].y() + 1, Qt::red);
		
	}

	for (int i = 2; i < body.size(); i++) {
		vektor.push_back(QPoint((body[i].x() - body[i - 2].x()) / 2.0, (body[i].y() - body[i - 2].y()) / 2.0));
	}

	update();
}

void QtClass::drawBSpline(){
	for (double t = 0; t < 1.; t += 0.001) {
		BSpline(t);

	}
	update();
}